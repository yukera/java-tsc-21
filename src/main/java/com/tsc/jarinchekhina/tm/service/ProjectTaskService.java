package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.repository.IProjectRepository;
import com.tsc.jarinchekhina.tm.api.repository.ITaskRepository;
import com.tsc.jarinchekhina.tm.api.service.IProjectTaskService;
import com.tsc.jarinchekhina.tm.entity.Project;
import com.tsc.jarinchekhina.tm.entity.Task;
import com.tsc.jarinchekhina.tm.exception.AbstractException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyIdException;
import com.tsc.jarinchekhina.tm.exception.entity.TaskNotFoundException;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import com.tsc.jarinchekhina.tm.util.DataUtil;

import java.util.List;
import java.util.Optional;

public class ProjectTaskService implements IProjectTaskService {

    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;

    public ProjectTaskService(final ITaskRepository taskRepository, final IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Task> findAllTaskByProjectId(final String userId, final String projectId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(projectId)) throw new EmptyIdException();
        return taskRepository.findAllByProjectId(userId, projectId);
    }

    @Override
    public Optional<Project> removeProjectById(final String userId, final String projectId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(projectId)) throw new EmptyIdException();
        final Optional<Project> project = projectRepository.findById(userId, projectId);
        if (!project.isPresent()) throw new AccessDeniedException();
        taskRepository.removeAllByProjectId(projectId);
        return projectRepository.removeById(projectId);
    }

    @Override
    public Task bindTaskByProjectId(final String userId, final String projectId, final String taskId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(projectId)) throw new EmptyIdException();
        if (DataUtil.isEmpty(taskId)) throw new EmptyIdException();
        final Optional<Task> task = taskRepository.findById(userId, taskId);
        if (!task.isPresent()) throw new TaskNotFoundException();
        final Optional<Project> project = projectRepository.findById(userId, projectId);
        if (!project.isPresent()) throw new AccessDeniedException();
        task.ifPresent(e -> e.setProjectId(projectId));
        return task.get();
    }

    @Override
    public Task unbindTaskByProjectId(final String userId, final String taskId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(taskId)) throw new EmptyIdException();
        final Optional<Task> task = taskRepository.findById(userId, taskId);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.ifPresent(e -> e.setProjectId(null));
        return task.get();
    }

    @Override
    public void clearProjects(final String userId) throws AccessDeniedException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        final List<Project> projects = projectRepository.findAll(userId);
        for (final Project project : projects) {
            String projectId = project.getId();
            taskRepository.removeAllByProjectId(projectId);
        }
        projectRepository.clear();
    }

}
