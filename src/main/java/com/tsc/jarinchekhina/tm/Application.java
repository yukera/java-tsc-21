package com.tsc.jarinchekhina.tm;

import com.tsc.jarinchekhina.tm.component.Bootstrap;
import com.tsc.jarinchekhina.tm.exception.AbstractException;

public class Application {

    public static void main(final String[] args) throws AbstractException {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}