package com.tsc.jarinchekhina.tm.util;

import java.util.Collection;

public interface DataUtil {

    static boolean isEmpty(final String value) {
        return (value == null || value.isEmpty());
    }

    static boolean isEmpty(final Collection value) {
        return (value == null || value.isEmpty());
    }

    static boolean isEmpty(final String[] values) {
        return (values == null || values.length == 0);
    }

    static boolean isEmpty(final Integer value) {
        return (value == null || value < 0);
    }

}
