package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.api.repository.IUserRepository;
import com.tsc.jarinchekhina.tm.entity.Project;
import com.tsc.jarinchekhina.tm.entity.User;

import java.util.Optional;
import java.util.function.Predicate;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    private Predicate<User> predicateByLogin (String login) {
        return e -> login.equals(e.getLogin());
    }

    private Predicate<User> predicateByEmail (String email) {
        return e -> email.equals(e.getEmail());
    }

    @Override
    public Optional<User> findByLogin(final String login) {
        return entities.stream()
                .filter(predicateByLogin(login))
                .findFirst();
    }

    @Override
    public Optional<User> findByEmail(final String email) {
        return entities.stream()
                .filter(predicateByEmail(email))
                .findFirst();
    }

    @Override
    public Optional<User> removeByLogin(final String login) {
        Optional<User> user = findByLogin(login);
        user.ifPresent(e -> entities.remove(e));
        return user;
    }

}
