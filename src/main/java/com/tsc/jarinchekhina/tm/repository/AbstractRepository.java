package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.api.IRepository;
import com.tsc.jarinchekhina.tm.entity.AbstractEntity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected Predicate<E> predicateById (String id) {
        return e -> id.equals(e.getId());
    }

    protected List<E> entities = new ArrayList<>();

    @Override
    public List<E> findAll() {
        return entities;
    }

    @Override
    public E add(final E entity) {
        entities.add(entity);
        return entity;
    }

    @Override
    public void addAll(final Collection<E> collection) {
        collection.forEach(this::add);
    }

    @Override
    public Optional<E> findById(final String id) {
        return entities.stream()
                .filter(predicateById(id))
                .findFirst();
    }

    @Override
    public void clear() {
        entities.clear();
    }

    @Override
    public Optional<E> removeById(final String id) {
        final Optional<E> entity = findById(id);
        entity.ifPresent(this::remove);
        return entity;
    }

    @Override
    public Optional<E> remove(final E entity) {
        entities.remove(entity);
        return Optional.ofNullable(entity);
    }

}
