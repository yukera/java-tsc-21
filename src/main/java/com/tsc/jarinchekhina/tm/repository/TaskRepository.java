package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.api.repository.ITaskRepository;
import com.tsc.jarinchekhina.tm.entity.Task;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    private Predicate<Task> predicateByUser (final String userId) {
        return e -> userId.equals(e.getUserId());
    }

    private Predicate<Task> predicateByProject (final String projectId) {
        return e -> projectId.equals(e.getProjectId());
    }

    private Predicate<Task> predicateByName (final String name) {
        return e -> name.equals(e.getUserId());
    }

    @Override
    public void clear(final String userId) {
        entities.stream()
                .filter(predicateByUser(userId))
                .forEach(this::remove);
    }

    @Override
    public void removeAllByProjectId(final String projectId) {
        entities.stream()
                .filter(predicateByUser(projectId))
                .forEach(this::remove);
    }

    @Override
    public List<Task> findAll(final String userId) {
        return entities.stream()
                .filter(predicateByUser(userId))
                .collect(Collectors.toList());
    }

    @Override
    public List<Task> findAll(final String userId, final Comparator<Task> comparator) {
        return entities.stream()
                .filter(predicateByUser(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        return entities.stream()
                .filter(predicateByUser(userId))
                .filter(predicateByProject(projectId))
                .collect(Collectors.toList());
    }

    @Override
    public Task add(final String userId, final Task task) {
        task.setUserId(userId);
        entities.add(task);
        return task;
    }

    @Override
    public Optional<Task> findById(final String userId, final String id) {
        return entities.stream()
                .filter(predicateByUser(userId))
                .filter(predicateById(id))
                .findFirst();
    }

    @Override
    public Optional<Task> findByIndex(final String userId, final Integer index) {
        return entities.stream()
                .filter(predicateByUser(userId))
                .skip(index)
                .findFirst();
    }

    @Override
    public Optional<Task> findByName(final String userId, final String name) {
        return entities.stream()
                .filter(predicateByUser(userId))
                .filter(predicateByName(name))
                .findFirst();
    }

    @Override
    public Optional<Task> remove(final String userId, final Task task) {
        if (!userId.equals(task.getUserId())) return Optional.empty();
        entities.remove(task);
        return Optional.of(task);
    }

    @Override
    public Optional<Task> removeById(final String userId, final String id) {
        Optional<Task> task = findById(userId, id);
        task.ifPresent(e -> entities.remove(e));
        return task;
    }

    @Override
    public Optional<Task> removeByIndex(final String userId, final Integer index) {
        Optional<Task> task = findByIndex(userId, index);
        task.ifPresent(e -> entities.remove(e));
        return task;
    }

    @Override
    public Optional<Task> removeByName(final String userId, final String name) {
        Optional<Task> task = findByName(userId, name);
        task.ifPresent(e -> entities.remove(e));
        return task;
    }

}
