package com.tsc.jarinchekhina.tm.api;

import com.tsc.jarinchekhina.tm.entity.AbstractEntity;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyIdException;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface IRepository<E extends AbstractEntity> {

    List<E> findAll();

    E add(E entity);

    void addAll(Collection<E> collection);

    Optional<E> findById(String id);

    void clear();

    Optional<E> removeById(String id);

    Optional<E> remove(E entity);

}
