package com.tsc.jarinchekhina.tm.api.repository;

import com.tsc.jarinchekhina.tm.api.IRepository;
import com.tsc.jarinchekhina.tm.entity.User;

import java.util.Optional;

public interface IUserRepository extends IRepository<User> {

    Optional<User> removeByLogin(String login);

    Optional<User> findByLogin(String login);

    Optional<User> findByEmail(String email);

}
