package com.tsc.jarinchekhina.tm.command;

import com.tsc.jarinchekhina.tm.api.service.IServiceLocator;
import com.tsc.jarinchekhina.tm.exception.AbstractException;

public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;

    public AbstractCommand() {
    }

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String arg();

    public abstract String name();

    public abstract String description();

    public abstract void execute() throws AbstractException;

}
